# Poppy Bird

![A screnshot of the game](screenshot.png)

Poppy Bird is a sample game written using [Pop], an experimental 2D game
development framework for Raku.

The game is an extremely simplified clone of Flappy Bird, largely following
the equivalent guide in [Simple Game Tutorials]

[Pop]: https://gitlab.com/jjatria/pop
[Simple Game Tutorials]: https://simplegametutorials.github.io/love/bird
