## Pop examples

This repository holds a set of example applications using [Pop], an
experimental 2D game development framework for Raku.

The code in these examples aims to highlight different aspects of the
framework, and specially during these early stages of development, they
work as a testbed for the API, which is very much in progress.

In order to run the applications you'll need to have a copy of Pop available.
You can install it using `zef`:

    zef install https://gitlab.com/jjatria/pop.git

For more specific details on each application, consult their respective
README files.

[Pop]: https://gitlab.com/jjatria/pop
