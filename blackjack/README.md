# Blackjack

![A screnshot of the game](screenshot.png)

Blackjack is a simple version of the card game of the same name implemented in
Raku using [Pop]. The assets and motivation for this example comes from the
[Simple Game Tutorials] of the same name, although the implementation differs
sustantively.

## Controls

During your turn, press `h` to take a card ("hit") and `s` to stick with your
current hand ("stand").

Once the dealer's turn has ended, press the spacebar to start a new game.

The game can also be controlled with the mouse, by using the buttons in the UI.

## Notes

This example is noteworthy as a demonstration of a possible way to generate
sprites in software by combining parts from one or more image files.

[Pop]: https://gitlab.com/jjatria/pop
[Simple Game Tutorials]: https://simplegametutorials.github.io/love/blackjack
