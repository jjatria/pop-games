# Flowers

![A screnshot of the game](screenshot.png)

Flowers is a simple clone of Minesweeper, with a less war-focused theme,
implemented using [Pop]. This implementation was done following the guide
in [Simple Game Tutorials], and uses the assets provided therein.

[Pop]: https://gitlab.com/jjatria/pop
[Simple Game Tutorials]: https://simplegametutorials.github.io/love/flowers
