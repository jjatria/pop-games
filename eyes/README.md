# Eyes

![A screnshot of the game](screenshot.png)

Eyes is a simple clone of the classic `xeyes` application, implemented using
[Pop]. The idea came in part by the equivalent guide in [Simple Game
Tutorials].

[Pop]: https://gitlab.com/jjatria/pop
[Simple Game Tutorials]: https://simplegametutorials.github.io/love/eyes
